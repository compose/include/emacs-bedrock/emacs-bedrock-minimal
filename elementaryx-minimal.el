;;;  /$$$$$$$$ /$$                                               /$$                                   /$$   /$$
;;; | $$_____/| $$                                              | $$                                  | $$  / $$
;;; | $$      | $$  /$$$$$$  /$$$$$$/$$$$   /$$$$$$  /$$$$$$$  /$$$$$$    /$$$$$$   /$$$$$$  /$$   /$$|  $$/ $$/
;;; | $$$$$   | $$ /$$__  $$| $$_  $$_  $$ /$$__  $$| $$__  $$|_  $$_/   |____  $$ /$$__  $$| $$  | $$ \  $$$$/
;;; | $$__/   | $$| $$$$$$$$| $$ \ $$ \ $$| $$$$$$$$| $$  \ $$  | $$      /$$$$$$$| $$  \__/| $$  | $$  >$$  $$
;;; | $$      | $$| $$_____/| $$ | $$ | $$| $$_____/| $$  | $$  | $$ /$$ /$$__  $$| $$      | $$  | $$ /$$/\  $$
;;; | $$$$$$$$| $$|  $$$$$$$| $$ | $$ | $$|  $$$$$$$| $$  | $$  |  $$$$/|  $$$$$$$| $$      |  $$$$$$$| $$  \ $$
;;; |________/|__/ \_______/|__/ |__/ |__/ \_______/|__/  |__/   \___/   \_______/|__/       \____  $$|__/  |__/
;;;                                                                                          /$$  | $$
;;;                                                                                         |  $$$$$$/
;;;                                                                                          \______/

;;; Minimal init.el

;;; Contents:
;;;
;;;  - Basic settings
;;;  - Discovery aids
;;;  - Minibuffer/completion settings
;;;  - Interface enhancements/defaults
;;;  - Tab-bar configuration
;;;  - Theme
;;;  - Optional extras
;;;  - Built-in customization framework

(require 'use-package)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Elementaryx Customization Group
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgroup elementaryx nil
  "Customization options for the ElementaryX package."
  :link '(url-link "https://elementaryx.gitlabpages.inria.fr/")
  :group 'emacs)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Elementaryx Version
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;###autoload
(defun elementaryx-version ()
  "Return the version number of ElementaryX if called non-interactively, otherwise display it.
If `elementaryx-editor` is set, display it in the message as well."
  (interactive)
  (let ((elementaryx-version "2.2.0"))
    (if (called-interactively-p 'any)
        (if (bound-and-true-p elementaryx-editor)
            (message "ElementaryX %s %s" elementaryx-editor elementaryx-version)
          (message "ElementaryX %s" elementaryx-version))
      elementaryx-version)))

(defconst elementaryx-version (elementaryx-version))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Basic settings
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; If you want to turn off the welcome screen, uncomment this
;; (setq inhibit-splash-screen t)

(setq initial-major-mode 'fundamental-mode)  ; default mode for the *scratch* buffer
(setq display-time-default-load-average nil) ; this information is useless for most

;; Automatically reread from disk if the underlying file changes
(setq auto-revert-interval 1)
(setq auto-revert-check-vc-info t)
(global-auto-revert-mode)
;; Revert Dired and other buffers https://systemcrafters.net/emacs-from-scratch/the-best-default-settings/
(setq global-auto-revert-non-file-buffers t)
;; Revert pdf without query https://stackoverflow.com/questions/42330517/force-docview-mode-to-show-updated-file-without-confirmation
(setq revert-without-query '(".pdf"))

;; Save history of minibuffer
(savehist-mode)

;; Fix archaic defaults
(setq sentence-end-double-space nil)

;; Don't litter file system with *~ backup files; put them all inside
;; ~/.emacs.d/backup or wherever
(defun elementaryx--backup-file-name (fpath)
  "Return a new file path of a given file path.
If the new path's directories does not exist, create them."
  (let* ((backupRootDir (concat user-emacs-directory "/emacs-backup"))
         (filePath (replace-regexp-in-string "[A-Za-z]:" "" fpath )) ; remove Windows driver letter in path
         (backupFilePath (replace-regexp-in-string "//" "/" (concat backupRootDir filePath "~") )))
    (make-directory (file-name-directory backupFilePath) (file-name-directory backupFilePath))
    backupFilePath))
(setq make-backup-file-name-function 'elementaryx--backup-file-name)

;; Use the visible bell rather than the audible bell
(setq visible-bell t)

;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Frame-Titles.html
;; Inspired from https://hieuphay.com/doom-emacs-config/ for the org-roam management
;; Be aware that the frame title may be used frequently
;; In particular using dynamic evaluation (":eval") may be demanding
;; If you experience a slow down of Elementaryx on big files, consider using
;; a simple frame title instead:
;; (setq frame-title-format "GNU Emacs ElementaryX")
;; Profiling through "M-x profiler-start M-x profiler-stop M-x
;; profiler-report" followed by "TAB" navigation may be used for
;; a diagnostic.
;; More on: https://www.gnu.org/software/emacs/manual/html_node/elisp/Profiling.html

(setq frame-title-format
      '(""
	;; Highlight with a bullet modified buffers (but not special buffers starting with '*' such as "*GNU Emacs*" or "*scratch*})
        (:eval
         (if (and (buffer-modified-p)
		  (not (string-match-p "^\\*.*\\*$" (buffer-name)))
		  (not (minibufferp (buffer-name)))
		  )
             "● "
           ""))
	(:eval
         (if (and (boundp 'org-roam-directory)
                  (s-contains-p org-roam-directory (or buffer-file-name "")))
             (replace-regexp-in-string
              ".*/[0-9]*-?" "☰ "
              (subst-char-in-string ?_ ?  buffer-file-name))
           "%b"))
;;;; It seems that current-project is too slow; we comment out this part:
	;; (:eval
        ;;  (let ((current-project (project-current nil)))
        ;;    (if current-project
        ;;        (let ((tmp-project-name (project-name current-project)))
        ;;          (if tmp-project-name
        ;;              (concat " [" tmp-project-name "]")
        ;;            ""))
        ;;      "")))
        " - GNU Emacs ElementaryX")
      icon-title-format frame-title-format)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Discovery aids
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Show the help buffer after startup
;; (add-hook 'after-init-hook 'help-quick)
;; use "-f help-quick" at emacs startup instead

;; which-key: shows a popup of available keybindings when typing a long key
;; sequence (e.g. C-x ...)
(use-package which-key
  :demand t
  :config
  (which-key-mode)
  (which-key-add-key-based-replacements "C-c h" "help")
  (which-key-add-key-based-replacements "C-c h w" "which-key")
  (which-key-add-key-based-replacements "C-c" `("major-mode/elementaryx/personal" . "Major mode, Elementaryx and Personal keys"))
  :bind (("C-c h w t" . which-key-show-top-level)
	 ("C-c h w m" . which-key-show-major-mode)
	 ("C-c h w k" . which-key-show-keymap)
	 ("C-c h w d" . describe-bindings)
	 ("C-c h w D" . describe-personal-keybindings)
	 ))
;; TODO: Refine setup (e.g. check out https://github.com/meatcar/emacs.d?tab=readme-ov-file#which-key)

;; Refine the "Help" submenu of the menu-bar
(use-package easymenu
  :config
  (easy-menu-add-item nil
		      '("Help") ; menu into which we insert our item/submenu (submenu here)
		      ["Show All Commands" execute-extended-command :help "Run commands by name, also known as VSCode Command Palette. Note that M-x shall read Meta-x; it can be performed either with Alt-x (Maintaining Alt and pressing x) or Esc-x (Pressing Esc, releasing it, and then pressing x)"]
		      "Emacs Tutorial" ; Optional argument to position the added item before that one
		      )
  (easy-menu-add-item nil
		      '("Help") ; menu into which we insert our item/submenu (submenu here)
		      '("Which Key (Start Key Sequence)"
			["Start Key Sequence (Show Top Level)" which-key-show-top-level :help "Show (most) top-level key bindings"]
			["Show Top level for Major Mode" which-key-show-major-mode :help "Show top-level bindings in the map of the current major mode"]
			["Show Top level for Selected Keymap" which-key-show-keymap :help "Show the top-level bindings in a selected keymap"]
			"----"
			["Describe All Bindings" describe-bindings :help "Display a buffer showing a list of all defined keys, and their definitions"]
			["Describe ElementaryX/Personal Bindings" describe-personal-keybindings :help "Display all the elementaryx and personal keybindings"]
			"----"
			["Customize Which-Key" (customize-group 'which-key)])
		      "Emacs Tutorial" ; Optional argument to position the added item before that one
		      )
  (easy-menu-add-item nil
		      '("Help") ; menu into which we insert our item/submenu (submenu here)
		      "----"
		      "Emacs Tutorial" ; Optional argument to position the added item before that one
		      ))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Server
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; We start the server, which we name "elementaryx" unless
;; ELEMENTARYX_EMACS_SOCKET_NAME environment variable is provided.

;; We can then use emacsclient joining "elementaryx" server with:
;; `emacsclient -s elementaryx'
;;
;; In some case (such as remote- / cluster- / super- computers), the
;; default directory in which the socket is created `(/run/user/$(id
;; -u)/emacs/)' may not be writable. In this case, it is possible to
;; provide a full path:
;;
;; mkdir -p ~/.emacs-socket/ ;;
;; export ELEMENTARYX_EMACS_SOCKET_NAME=$HOME/.emacs-socket/elementaryx ;;
;; ;; server side:
;; ; start/elementaryx/as/you/wish;;
;; ;; client side
;; emacsclient -s $ELEMENTARYX_EMACS_SOCKET_NAME ;;
;;
;; Note that you may also override EMACS_SOCKET_NAME, in which case it
;; is not anymore necessary to explicitly provide the socket to
;; `emacsclient':
;;
;; mkdir -p $HOME/.emacs-socket/ ;;
;; export ELEMENTARYX_EMACS_SOCKET_NAME=$HOME/.emacs-socket/elementaryx ;;
;; export EMACS_SOCKET_NAME=$ELEMENTARYX_EMACS_SOCKET_NAME ;;
;; ;; server side:
;; ; start/elementaryx/as/you/wish;;
;; ;; client side
;; emacsclient ;;

;; It is possible to prevent starting the server: if the
;; ELEMENTARYX_EMACS_NO_SERVER environment variable is set, the server
;; will not start at all.

(use-package server
  :config
  ;; Configuration for starting the Emacs server

  ;; Check if the ELEMENTARYX_EMACS_NO_SERVER environment variable is set.
  ;; If this variable is set, the server will not start.
  ;; This allows you to disable the server through without modifying the Emacs configuration.
  (unless (getenv "ELEMENTARYX_EMACS_NO_SERVER")

    ;; Set the Emacs server name based on the ELEMENTARYX_EMACS_SOCKET_NAME environment variable if provided.
    ;; If ELEMENTARYX_EMACS_SOCKET_NAME is set, use its value; otherwise, default to "elementaryx".
    ;; This variable determines the name of the server and helps in managing multiple Emacs instances.
    (setq server-name
          (or (getenv "ELEMENTARYX_EMACS_SOCKET_NAME")  ;; Use the environment variable if set
              "elementaryx"))                           ;; Default to "elementaryx" if not set

    ;; Check if the server with the specified name is already running.
    ;; If not running, start the server with the configured name.
    ;; This ensures that you do not accidentally start multiple server instances with the same name.
    (unless (server-running-p server-name)
      (server-start))))  ;; Start the Emacs server if it is not already running

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Copy / Paste with X Interaction
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; When running as Graphical User Interface (GUI), typically in Linux
;; on top of X11 or Wayland, =emacs= natively handles Copy / Paste
;; interaction with other GUI applications through the X Clipboard.
;; However, in non terminal mode, there is no interaction with the X
;; Clipboard by default. We rely on the `emacs-xclip' package to do so
;; with the `xclip-mode' global minor mode.

;; `xclip-mode' neeeds a binary under the hood. Under X11 (or XWayland
;; compatibility Wayland layer for supporting X), it may `xclip'
;; (embedded in Elementaryx) or `xsel' (not embedded in ElementaryX).
;; Wayland has also native support through `wl-clipboard' (not
;; embedded in ElementaryX).

;; The DISPLAY$ environment variable must be preserved. Otherwise the
;; paste operation will seek for it and retrieve: "Error: Can't open
;; display [...]". In some cases the `XAUTHORITY' environment variable
;; must be preserved as well, but in general it is not necessary as it
;; it is equal to the default ~/.Xauthority.

;; When `emacs' is started on a remote computer with `ssh', X11 must
;; be preserved (and possibly trusted), e.g. by setting within
;; `~/.ssh/config': `ForwardX11 yes' (and possibly `ForwardX11Trusted
;; yes').

;; Note that the host (the machine from which we do `ssh' ) must have
;; some setup. At this stage the exact requirement is stil WIP (shall
;; we rely on the `wayland' binary from the `wayland' guix package on
;; Wayland?).

;; The full setup is not trivial to achieve. We therefore
;; load the package conditionally:
;; - the DISPLAY environment variable must be set
;; - we must be able to perform an `xclip -selection' to the clipboard

;; On remote computers (typically accessed through =ssh=), the
;; synchronization with the X clipboard of the host may be slow
;; depending on the network connection. This is not a problem for the
;; copy operation which is asynchronous. On the contrary, the paste
;; operation is synchronous. If the connection is slow, the latency
;; induced by the synchronization with the clipboard may be annoying.
;; We therefore do not bind the paste operation (which remains
;; available through `C-c C-v'). Therefore, if we detect ElementaryX
;; is started within an SSH connection (we check whether the
;; `SSH_CONNECTION' environment variable is set up), the paste
;; operation is not handled by `xclip' (except for the primary
;; selection case). This is done by refining the
;; `gui-backend-get-selection' method making sure it is called with
;; `xclip-mode' (temporarily) equal to `nil'.

(use-package xclip
  :if (and (getenv "DISPLAY")
           (eq (call-process-shell-command "echo 'Test' | xclip -selection clipboard") 0))
  :config
  ;; 1. Uggly fix:
  ;; It seems that (setq xterm-extra-capabilities '(setSelection)) may
  ;; not be sufficient. We therefore apply a uggly Hack where we
  ;; directly override `display-selections-p'. We assume that we can
  ;; do so as we the above `:if' test was successful.
  ;; Directly force `display-selections-p' to return t:
  (advice-add 'display-selections-p :override
	      (lambda (&optional display) t))
  ;; Note that the altered behaviour can be rolled back using:
  ; (advice-remove 'display-selections-p (lambda (&optional display) t))))
  ;; 2. Do not apply xclip for get-selection (i.e. paste) in the SSH
  ;; case, except for primary selection
  (if (getenv "SSH_CONNECTION")
      ;; 2.1 SSH case
      (progn
	(message "xclip: loading for copy only (and restricted to primary selection in the paste because of potential latency running over ssh).")
	;; `xclip' redefines the `gui-backend-get-selection' method
	;; (from `select.el'). We redefine it as well, locally
	(cl-defmethod gui-backend-get-selection ((selection-symbol symbol) _target-type
						 &context (window-system nil))
	  (if (memq selection-symbol '(primary PRIMARY))
	      ;; `type` is 'primary or 'PRIMARY
	      (progn
		(cl-call-next-method))
	    ;; `type` is 'secondary, 'SECONDARY, 'clipboard or 'CLIPBOARD
	    (let ((xclip-mode nil))     ;; Temporarily set xclip-mode to
	                                ;; nil.
	      (cl-call-next-method))))) ;; Call the next method. If the
	                                ;; next method is the xclip one,
				        ;; because xclip-mode is nil in
				        ;; this scope, it shall in turn
				        ;; call the second next method.
    (progn
      ;; 2.2 Non-SSH case
      (message "xclip: loading and activating for copy and paste.")))
  ;; 3. Turn on xclip-mode
  (xclip-mode 1))

;; For debugging purpose, note that `xauth -n list' allows one to get
;; the list of xauth cookies by IP (thanks to `-n'), which may be
;; related to $DISPLAY.

;; Note that (see https://lists.nongnu.org/archive/html/emacs-devel/2022-06/msg00126.html )
;; the expected behaviour would be that setting
;; - `(setq xterm-extra-capabilities '(setSelection))'
;; - `(setq tty-select-active-regions t)'
;; are enough to have `display-selections-p' return t.
;; But it's not the case, hence the above uggly hack defining:
;; (defun display-selections-p (&optional display) t)
;; , which we do with `advice-add'.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Mouse support
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Make right-click do something sensible We do it whether or
;; `display-graphic-p' predicate is true as in any case we'll enable
;; mouse (through xterm-mouse-mode in no window mode)
(context-menu-mode)

;; Enable xterm-mouse-mode in non-graphical environment and not in batch mode
;; Text-only mouse in emacs: https://www.gnu.org/software/emacs/manual/html_node/emacs/Text_002dOnly-Mouse.html
;; We should check with display-graphic-p predicate rather than window-system variable: https://www.gnu.org/software/emacs/manual/html_node/elisp/Window-Systems.html ; however, if emacs is started with "-nw" in a context with display graphic capacity, the display-graphic-p predicate will be true. We therefore also check the deprecated window-system variable.
;; Check the batch mode with noninteractive variable: https://www.gnu.org/software/emacs/manual/html_node/elisp/Batch-Mode.html
(when (and (or (not (display-graphic-p)) (null window-system))
           (not noninteractive))
  (xterm-mouse-mode 1))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Minibuffer/completion settings
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; For help, see: https://www.masteringemacs.org/article/understanding-minibuffer-completion

(setq enable-recursive-minibuffers t)                ; Use the minibuffer whilst in the minibuffer
(setq completion-cycle-threshold 1)                  ; TAB cycles candidates
(setq completions-detailed t)                        ; Show annotations
(setq tab-always-indent 'complete)                   ; When I hit TAB, try to complete, otherwise, indent
(setq completion-styles '(basic initials substring)) ; Different styles to match input to candidates

(setq completion-auto-help 'always)                  ; Open completion always; `lazy' another option
(setq completions-max-height 20)                     ; This is arbitrary
(setq completions-detailed t)
(setq completions-format 'one-column)
(setq completions-group t)
(setq completion-auto-select 'second-tab)            ; Much more eager
;(setq completion-auto-select t)                     ; See `C-h v completion-auto-select' for more possible values

(keymap-set minibuffer-mode-map "TAB" 'minibuffer-complete) ; TAB acts more like how it does in the shell

;; For a fancier built-in completion option, try ido-mode or fido-mode. See also
;; the file extras/base.el
;(fido-vertical-mode)
;(setq icomplete-delay-completions-threshold 4000)


;;;;;;;;;;;;;;
;; Warnings ;;
;;;;;;;;;;;;;;

;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Warning-Options.html

;; We request that only error-level warnings get displayed, but not
;; warning-level ones (as it is the case otherwise by default in
;; emacs):
(setq warning-minimum-level :error)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Major key-binding changes ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;; Windows-like copy (C c), cut (C x), paste (C v), cancel (C z)
;; (cua-mode)
;; Not enabled by default. Use "-f cua-mode" at emacs startup instead

;; Directional window change
;; Change window through "CTRL + SUPER + <arrowkey>" (SUPER being "Window" key on Windows/Linux and "Command" key on Mac OS)
(use-package windmove
  :config
  ;; (setq windmove-wrap-around t)
  ;; WIND move keybindings:
  ;; (windmove-default-keybindings '(shift))
  (global-set-key (kbd "C-s-<left>")  'windmove-left)
  (global-set-key (kbd "C-s-<right>") 'windmove-right)
  (global-set-key (kbd "C-s-<up>")    'windmove-up)
  (global-set-key (kbd "C-s-<down>")  'windmove-down))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Interface enhancements/defaults
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Mode line information
(setq line-number-mode t)                        ; Show current line in modeline
(setq column-number-mode t)                      ; Show column as well

(setq x-underline-at-descent-line nil)           ; Prettier underlines
(setq switch-to-buffer-obey-display-actions t)   ; Make switching buffers more consistent

(setq-default show-trailing-whitespace t)        ; Underline trailing spaces
(setq-default indicate-buffer-boundaries 'left)  ; Show buffer top and bottom in the margin

;; Enable horizontal scrolling
(setq mouse-wheel-tilt-scroll t)
(setq mouse-wheel-flip-direction t)

;; We won't set these, but they're good to know about
;;
;; (setq-default indent-tabs-mode nil)
;; (setq-default tab-width 4)

;; Misc. UI tweaks
;; (blink-cursor-mode -1)                             ; Steady cursor
(pixel-scroll-precision-mode)                         ; Smooth scrolling

;; Keep track of recently opened file
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/File-Conveniences.html
(recentf-mode)
;; More following spacemacs spirit:
(add-hook 'find-file-hook (lambda () (unless recentf-mode
                                       (recentf-mode)
                                       (recentf-track-opened-file))))
(setq recentf-max-saved-items 1000
      recentf-auto-cleanup 'never
      ;; recentf-save-file (concat spacemacs-cache-directory "recentf")
      recentf-auto-save-timer (run-with-idle-timer 600 t
                                                   'recentf-save-list))


;; Display line numbers in programming mode
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(setq-default display-line-numbers-width 3)           ; Set a minimum width

;; Nice line wrapping when working with text
(add-hook 'text-mode-hook 'visual-line-mode)

;; Modes to highlight the current line with
(let ((hl-line-hooks '(text-mode-hook prog-mode-hook)))
  (mapc (lambda (hook) (add-hook hook 'hl-line-mode)) hl-line-hooks))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Tab-bar configuration
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Show the tab-bar as soon as tab-bar functions are invoked
(setq tab-bar-show 0)

;; Add the time to the tab-bar, if visible
(add-to-list 'tab-bar-format 'tab-bar-format-align-right 'append)
(add-to-list 'tab-bar-format 'tab-bar-format-global 'append)
(setq display-time-format "%a %F %T")
(setq display-time-interval 1)
(display-time-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Some basic keybindings ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package emacs
  :preface
  ;; Define an interactive function to open files with xdg-open
  (defun elementaryx-open-with-xdg-open (file)
    "Open FILE with xdg-open."
    (interactive "Open file with xdg-open: ")
    (call-process "xdg-open" nil 0 nil file))
  (defun goto-init-directory ()
    (interactive)
    (find-file user-emacs-directory))
  (defun elementaryx-global-text-scale-increase ()
    "Increase the global text size. Keyboard binding: `C-+'. Mouse binding: `C-wheel-up'."
    (interactive)
    (global-text-scale-adjust 1))
  (defun elementaryx-global-text-scale-decrease ()
    "Deacrease the global text size. Keyboard binding: `C--'. Mouse binding: `C-wheel-down'."
    (interactive)
    (global-text-scale-adjust 1))
  :bind (:map global-map
	      ("C-c o" . elementaryx-open-with-xdg-open)
              ("C-+" . text-scale-increase)
              ("C--" . text-scale-decrease)
	      ("C-M-+" . elementaryx-global-text-scale-increase)
	      ("C-M--" . elementaryx-global-text-scale-decrease)
              ;; ("C-M-+" . (lambda () (interactive) (global-text-scale-adjust 1)))
              ;; ("C-M--" . (lambda () (interactive) (global-text-scale-adjust -1)))
	      ("M-g d" . goto-init-directory))
  :init
  (easy-menu-add-item nil
		      '("Tools") ; Menu into which inserting
		      '("Zoom"
			["Zoom In (Global)"  elementaryx-global-text-scale-increase :help "Increase the font size of all faces. Keyboard binding: `C-M-+'. Mouse binding: `C-M-wheel-up'."]
			["Zoom Out (Global)" elementaryx-global-text-scale-decrease :help "Decrease the font size of all faces. Keyboard binding: `C-M--'. Mouse binding: `C-M-wheel-down'."]
			["Zoom In (Buffer)"  text-scale-increase :help "Increase the font size of the default face in current buffer. Keyboard binding: `C-+'. Mouse binding: `C-wheel-up'."]
			["Zoom Out (Buffer)"  text-scale-decrease :help "Increase the font size of the default face in current buffer. Keyboard binding: `C--'. Mouse binding: `C-wheel-down'."]
			)
		      "Read Net News" ; Insert before this item/submenu
		      )
  (easy-menu-add-item nil
		      '("Tools") ; Menu into which inserting
		      "----"
		      "Read Net News" ; Insert before this item/submenu
		      )

  (easy-menu-add-item nil
		      '("File")		; Insert within the Edit menu
		      ["Open Emacs Initialization Directory" goto-init-directory :help "Open Emacs initialization directory, possibly overriden at emacs startup with `--init-directory='"]
		      "Insert File..." ; Insert before this item/submenu
		      )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;   Theme
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package emacs
  :config
  (load-theme 'modus-vivendi))          ; for light theme, use modus-operandi

(provide 'elementaryx-minimal)
